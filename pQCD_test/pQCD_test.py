import numpy as np

from pQCD import pQCD

mu_grid = np.linspace(2.0,3,100) #Grid in chemical potential in [GeV]

pQCD1 = pQCD(1/2) #X=1/2 
pQCD2 = pQCD(2) #X=2 

from pQCD import pQCD
pQCD = pQCD(1)

eos1 = np.genfromtxt("QMH_pQCD.dat", names=True, skip_header=1)

(ptest, etest, nbtest) = (eos1['pressure']/10**3, eos1['energy_density']/10**3, eos1['baryon_density'])



with open("pQCDtest.dat", "w") as file:
    file.write(str(pQCD.constraints(e0=etest,p0=ptest,n0=nbtest)))

