# Nuclear and Hybrid Equations of State in Light of the Low-Mass Compact Star in HESS J1731-347

This is the public code to compute all results presented in https://arxiv.org/abs/2302.02989

If you keep the file structure as is, the Mathematica notebook "superlight_hybridstar.nb" will be executable from top to bottom. Every section can be executed by itself by loading intermediate results. Some of the computations are time consuming, it is recommended to load the results of these calculations. All results are provided in the "data" directory.