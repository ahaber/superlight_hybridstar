(* ::Package:: *)

Needs["ClassDef`"];

ClassDef`BeginClass["TOV",
 (* Needed packages: *)
 {"Constants`","InterpolationTools`","NumericalCalculus`"},

 (* data members: *)
 {"p$central",
  "Edensity", (* function giving the EoS e[p] of the matter in the star *)
  "EdensityDeriv",
  "EdensitySecondDeriv",
  "r$of$p$IF", (* 'IF' means interpolating fn returned by NDSolve *)
  "p$of$r$IF",
  "M$of$p$IF",
  "radius","mass",  (* radius and mass in MeV *)
  "radius$km","mass$solar",   (* R and M in usual astro units *)
  "r$int","p$int","M$int",  (* analytic approx will be used at r<r$int *)
  "p$surface"  (* regulator: NDSolve may not be able to go all the way to p=0 *)
 },

 (* member functions (other than create$TOV and is$TOV): *)
 {"solve$TOV",  
 "PrintProperties", 
 "p$approx",
 "r$approx",
 "M$approx",
 "plot$TOV",
 "probe$TOV",
 "p$of$r",
 "r$of$p",
 "M$of$r",
 "M$of$p", 
 "e$of$r",
 "dedp",
 "dedr",
 "d2edr2",
 "dpdr",
 "d2pdr2",
 "dM$dr$of$rkm",
 "de$dr$of$rkm",
 "lambda$of$rkm",  
 "nu$of$rkm", 
 "e$to$the$nu",
 "e$to$the$lambda",
 "Gamma$of$rkm",
 "Gamma$of$r",
 "dGamma$dr$of$rkm",
 "dGamma$dr$of$r",
 "W$of$rkm",
 "W$of$r",
 "W$of$r$times$rMeV3",
 "Q$of$r",
 "Q$of$rkm",
 "Q$of$r$times$rMeV3",
 "Pi$of$r",
 "Pi$of$rkm",
 "Pi$of$r$times$rMeV3",
 "dPi$dr",
 "dPi$dr$times$rMeV3",
 "check$causal",
 "check$causal$pvals"
 }
];

create$TOV[p$central$val_?NumericQ, Edensityfn_]:=Module[
 {self},
 self=create$TOV[]; (* call default constructor, defined in ClassDef *)
 set$Edensity[self,Edensityfn];
 set$EdensityDeriv[self,Derivative[1][Edensityfn]];
 set$EdensitySecondDeriv[self, Derivative[2][Edensityfn]];
 set$p$central[self,p$central$val];
 (* Results of solving TOV: *)
 set$r$of$p$IF[self,Null];  (* interpolating fn from NDSolve *)
 set$M$of$p$IF[self,Null];  (* interpolating fn from NDSolve *)
 set$radius[self,0];
 set$mass[self,0];
 set$radius$km[self,0];
 set$mass$solar[self,0];
 (* size etc of the inner region where we solve TOV analytically: *)
 set$r$int[self,0];
 set$M$int[self,0];
 set$p$int[self,0];
 (* pressure of surface: (should be zero, but may need small finite value
    like 10.0^-15  for numerical reasons) *)
 set$p$surface[self,0];
 (*check$causal[self,p$central$val];*)
 self
];

p$of$r[tov_?is$TOV,r_?NumericQ]:=Module[{p},
 (* pressure as a fn of radius *)
 If[r<=r$int[tov], p$approx[tov,r]
  ,(*else*)
  If[r>radius[tov], 0
  ,(*else*)
   If[ ! in$domain[p$of$r$IF[tov],r,"TOV`p$of$r$IF"], Abort[]];
   p$of$r$IF[tov][r]
  ]
 ]
];

r$of$p[tov_?is$TOV,p_?NumericQ]:=Module[{r},
 If[p>p$central[tov],
  Print["r$of$p: p greater than central value."]
  0
 ,(*else*)
  If[p>=p$int[tov], r$approx[tov,p]
  ,(*else*)
   If[p>p$surface[tov], r$of$p$IF[tov][p]
   ,(*else*)
    If[ False&&!in$domain[r$of$p$IF[tov],p,"TOV`r$of$p$IF"], Abort[] ];
    r$of$p$IF[tov][p$surface[tov]]
   ]
  ]
 ]
];

M$of$p[tov_?is$TOV,p_?NumericQ]:=Module[{M},
 If[p>p$central[tov],
  Print["r$of$p: p greater than central value."]
  0
 ,(*else*)
  If[p>=p$int[tov], M$approx[tov,r$approx[tov,p]]
  ,(*else*)
   If[p>p$surface[tov], M$of$p$IF[tov][p]
   ,(*else*)
    If[ False&&!in$domain[M$of$p$IF[tov],p,"TOV`M$of$p$IF"], Abort[] ];
    M$of$p$IF[tov][p$surface[tov]]
   ]
  ]
 ]
];

M$of$r[tov_?is$TOV,r_?NumericQ]:=Module[{M},
 (* Enclosed mass/energy as a fn of radius *)
 If[r<=r$int[tov], M$approx[tov,r]
  ,(*else*)
  If[r>radius[tov], mass[tov]
  ,(*else*)
   M$of$p$IF[tov][p$of$r[tov,r]]
  ]
 ]
];

e$of$r[tov_?is$TOV,r_?NumericQ]:=Module[{M},
 (* energy density as a fn of radius *)
 If[r<=r$int[tov], Edensity[tov][p$approx[tov,r]]
  ,(*else*)
  If[r>radius[tov], 0
  ,(*else*)
   Edensity[tov][p$of$r[tov,r]]
  ]
 ]
];

dedp[tov_?is$TOV, r_?NumericQ]:=Module[{p, pr},
 p = p$of$r[tov, r];
 EdensityDeriv[tov][p]
];

dpdr[tov_?is$TOV, r_?NumericQ]:=Module[{pr,Mr,dp$dr}, 
 If[r == 0, 
  dp$dr = 0;
 , (* else *)
  pr=p$of$r[tov,r]; 
  Mr=M$of$r[tov,r]; 
  dp$dr = -NewtonG/(r^2)*(e$of$r[tov,r] + pr)*(Mr + 4*Pi*r^3*pr)/(1-2*NewtonG*Mr/r);
 ];
 dp$dr
];

dedr[tov_?is$TOV, r_?NumericQ] := Module[{de$dp, dp$dr},
 de$dp = dedp[tov, r];
 dp$dr = dpdr[tov, r];
 de$dp * dp$dr
];

de$dr$of$rkm[tov_?is$TOV,rkm_?NumericQ]:=Module[{},
 dedr[tov, rkm/MeV$km];
];

dM$dr$of$rkm[tov_?is$TOV,rkm_?NumericQ]:=Module[{edens, r},
 r = rkm/MeV$km;
 edens = e$of$r[tov, rkm/MeV$km];
 4 * Pi * edens * r^2  
];


lambda$of$rkm[tov_?is$TOV,rkm_?NumericQ]:=Module[{Mr$km, lambda},
 If[rkm == 0,
  lambda = 0;
 , (* else *)
  Mr$km = M$of$r[tov,rkm/MeV$km] * NewtonG$km$per$MeV; (* convert M (r) to km *)
  lambda = -(1/2)Log[1-2*Mr$km/rkm];
 ];
 lambda
];  

nu$of$rkm[tov_?is$TOV,rkm_?NumericQ]:=Module[{e$nu},
 e$nu = TOV`e$to$the$nu[tov, rkm/MeV$km];
 Log[e$nu] 
];

e$to$the$nu[tov_?is$TOV, r2_?NumericQ] := Module[{},
 If[r2 <= 1*^-12,
	Exp[0.5 * Log[(1 - 2(TOV`mass[tov] * NewtonG)/(TOV`radius[tov]))] -  NIntegrate[(M$of$r[tov, r] + 4 Pi r^3 (TOV`p$of$r[tov, r]))/
			(r (r/NewtonG - 2 M$of$r[tov, r])), {r,1*^-12, TOV`radius[tov]}]],
	(* else *)
    Exp[0.5 * Log[(1 - 2(TOV`mass[tov] * NewtonG)/(TOV`radius[tov]))] -  NIntegrate[(M$of$r[tov, r] + 4 Pi r^3 (TOV`p$of$r[tov, r]))/
			(r (r/NewtonG - 2 M$of$r[tov, r])), {r,r2, TOV`radius[tov]}]]
 ]
];

e$to$the$lambda[tov_?is$TOV, r_?NumericQ] := Module[{m},
 If[r == 0,
  1,
 (* else *)
  m = M$of$r[tov, r];
  Sqrt[1/(1 - (2 * NewtonG * m)/r)]
 ]
];

Gamma$of$rkm[tov_?is$TOV,rkm_]:=Module[{pressure,pr,edens,dp$de},
 pressure = p$of$r[tov,rkm/MeV$km]; 
 edens = Edensity[tov][pressure];
 dp$de = (dedp[tov, rkm/MeV$km])^(-1); (* pressure gradient *)
 (* we are calculating a dimensionless quantity, so units don't matter *)
 (edens+pressure)/(pressure) * dp$de
];

Gamma$of$r[tov_?is$TOV, r_?NumericQ] := Module[{e, p, dpde},
  p = p$of$r[tov, r];
  e = e$of$r[tov, r];
  dpde = (dedp[tov, r])^(-1);
  (e/p + 1) * dpde
];

dGamma$dr$of$rkm[tov_?is$TOV, rkm_?NumericQ] := Module[{edens, p, dp$dr, de$dr, dp$de, r},
 r = rkm/MeV$km;
 edens = e$of$r[tov, r];
 p = TOV`p$of$r[tov, r]; 
 dp$dr = TOV`dpdr[tov, r];
 de$dr = TOV`dedr[tov, r];
 dp$de = (TOV`dedp[tov, r])^(-1);
 -(dp$de)^(2) * EdensitySecondDeriv[tov][p] * dp$dr * (edens/p + 1) + dp$de * (de$dr * (1/p) - dp$dr * edens/(p^2))
];

dGamma$dr$of$r[tov_?is$TOV, r_?NumericQ] := Module[{edens, p, dp$dr, de$dr, dp$de},
 edens = e$of$r[tov, r];
 p = TOV`p$of$r[tov, r]; 
 dp$dr = TOV`dpdr[tov, r];
 de$dr = TOV`dedr[tov, r];
 dp$de = (TOV`dedp[tov, r])^(-1);
 -(dp$de)^(2) * EdensitySecondDeriv[tov][p] * dp$dr * (edens/p + 1) + dp$de * (de$dr * (1/p) - dp$dr * edens/(p^2))
];


Pi$of$r[tov_?is$TOV,  r_?NumericQ] := Module[{edens, elambda, enu, gamma, pr, gammapr},
 If[r < radius[tov],
  elambda = e$to$the$lambda[tov, r];
  enu = e$to$the$nu[tov, r];
  gamma = Gamma$of$r[tov, r];
  pr = p$of$r[tov, r]; 
  elambda * enu^3 * r^(-2) * gamma * pr
 ,(* else *)
  edens = e$of$r[tov, r];
  elambda = e$to$the$lambda[tov, r];
  enu = e$to$the$nu[tov, r];
  pr = p$of$r[tov, r];
  gammapr = (edens + pr) * EdensityDeriv[tov][pr];
  elambda * enu^3 * r^(-2) * gammapr
 ]
];

Pi$of$rkm[tov_?is$TOV, rkm_?NumericQ] := Module[{edens, elambda, enu, gamma, pr, r, gammapr},
 If[rkm <= radius$km[tov],
  r = rkm/MeV$km;
  elambda = e$to$the$lambda[tov, r];
  enu = e$to$the$nu[tov, r];
  gamma = Gamma$of$rkm[tov, rkm];
  pr = p$of$r[tov, r]; 
  elambda * enu^3 * r^(-2) * gamma * pr
 ,(* else *)
  r = rkm/MeV$km;
  edens = e$of$r[tov, r];
  elambda = e$to$the$lambda[tov, r];
  enu = e$to$the$nu[tov, r];
  gammapr = (edens + pr) * EdensityDeriv[tov][pr];
  elambda * enu^3 * r^(-2) * gammapr
 ]
];

Pi$of$r$times$rMeV3[tov_?is$TOV,r_?NumericQ] := Module[{edens, elambda, enu, gamma, pr, gammapr},
 If[r < radius[tov],
  elambda = e$to$the$lambda[tov, r];
  enu = e$to$the$nu[tov, r];
  gamma = Gamma$of$r[tov, r];
  pr = p$of$r[tov, r]; 
  elambda * enu^3 * r * gamma * pr
 ,(* else *)
  edens = e$of$r[tov, r];
  elambda = e$to$the$lambda[tov, r];
  enu = e$to$the$nu[tov, r];
  pr = p$of$r[tov, r];
  gammapr = (edens + pr) * EdensityDeriv[tov][pr];
  elambda * enu^3 * r * gammapr
 ]
];

dPi$dr[tov_?is$TOV, r_?NumericQ] := Module[{elambda, enu, gamma, pr, m, dmdr, dgamma$dr, dp$dr, e, dpde, dedr, d2edp2},
 If[r < radius[tov],
  elambda = e$to$the$lambda[tov, r];
  enu = e$to$the$nu[tov, r];
  gamma = Gamma$of$r[tov, r];
  pr = TOV`p$of$r[tov, r]; 
  m = M$of$r[tov, r];
  dmdr = dM$dr$of$rkm[tov, r * MeV$km];
  dp$dr = TOV`dpdr[tov, r];
  dgamma$dr = dGamma$dr$of$r[tov, r];
  e = e$of$r[tov,r];
  ((1/(r - 2 * m * NewtonG) * dmdr + ((2 * m + 12 * Pi * r^3 * pr)/(r (r - 2 * m * NewtonG)))) * NewtonG * elambda * enu^3 * r^(-2) * gamma* pr)
    - 2 * elambda * r^(-3) enu^3 * gamma*pr
    + elambda * enu^3 * r^(-2) * pr * dgamma$dr
    + elambda * enu^3 * r^(-2) * gamma * dp$dr
  , (* else *)
  elambda = e$to$the$lambda[tov, r];
  enu = e$to$the$nu[tov, r];
  m = TOV`mass[tov];
  e = e$of$r[tov, r];
  pr = 0;
  dmdr = 4 * Pi * e * r^2;
  dp$dr = dpdr[tov, r];
  dpde = EdensityDeriv[tov][pr]^(-1);
  dedr = EdensityDeriv[tov][pr] * dp$dr;
  d2edp2 = EdensitySecondDeriv[tov][pr];
  ((1/(r - 2 * m * NewtonG) * dmdr + ((2 * m + 12 * Pi * r^3 * pr)/(r (r - 2 * m * NewtonG)))) * NewtonG * elambda * enu^3 * r^(-2) * (e + pr) * dpde)
    - 2 * elambda * r^(-3) enu^3 * (e + pr) * EdensityDeriv[tov][pr]^(-1)
    + elambda * enu^3 * r^(-2) * ((dedr + dp$dr) * dpde + (e + pr)(dpde)^2(d2edp2)(dp$dr))
 ]
];

dPi$dr$times$rMeV3[tov_?is$TOV, r_?NumericQ] := Module[{elambda, enu, gamma, pr, m, dmdr, dgamma$dr, dp$dr, e, dpde, dedr, d2edp2},
 If[r == 0,
  elambda = e$to$the$lambda[tov, r];
  enu = e$to$the$nu[tov, r];
  pr = p$of$r[tov, r];
  e = e$of$r[tov, r];
  dpde = EdensityDeriv[tov][pr]^(-1);
  -2 * elambda * enu^3 * (e + pr) * dpde
 , (* else *)
  If[r < radius[tov],
   elambda = e$to$the$lambda[tov, r];
   enu = e$to$the$nu[tov, r];
   gamma = Gamma$of$r[tov, r];
   pr = TOV`p$of$r[tov, r]; 
   m = M$of$r[tov, r];
   dmdr = dM$dr$of$rkm[tov, r * MeV$km];
   dp$dr = TOV`dpdr[tov, r];
   dgamma$dr = dGamma$dr$of$r[tov, r];
   e = e$of$r[tov,r];
   ((1/(r - 2 * m * NewtonG) * dmdr + ((2 * m + 12 * Pi * r^3 * pr)/(r (r - 2 * m * NewtonG)))) * NewtonG * elambda * enu^3 * r * gamma* pr)
     - 2 * elambda * enu^3 * gamma*pr
     + elambda * enu^3 * r * pr * dgamma$dr
     + elambda * enu^3 * r * gamma * dp$dr
   , (* else *)
   elambda = e$to$the$lambda[tov, r];
   enu = e$to$the$nu[tov, r];
   m = TOV`mass[tov];
   e = e$of$r[tov, r];
   pr = 0;
   dmdr = 4 * Pi * e * r^2;
   dp$dr = dpdr[tov, r];
   dpde = EdensityDeriv[tov][pr]^(-1);
   dedr = EdensityDeriv[tov][pr] * dp$dr;
   d2edp2 = EdensitySecondDeriv[tov][pr];
   ((1/(r - 2 * m * NewtonG) * dmdr + ((2 * m + 12 * Pi * r^3 * pr)/(r (r - 2 * m * NewtonG)))) * NewtonG * elambda * enu^3 * r * (e + pr) * dpde)
     - 2 * elambda * enu^3 * (e + pr) * dpde
     + elambda * enu^3 * r * ((dedr + dp$dr) * dpde + (e + pr)(dpde)^2(d2edp2)(dp$dr))
  ]
 ]
];



W$of$rkm[tov_?is$TOV,rkm_?NumericQ] := Module[{enu, elambda, pr, edens, r},
 If[rkm <= radius$km[tov],
  r = rkm/MeV$km;
  enu = e$to$the$nu[tov, r];
  elambda = e$to$the$lambda[tov, r];
  pr = p$of$r[tov, r];
  edens = e$of$r[tov, r];   
  elambda^3 * enu * r^(-2) * (edens + pr)
 ,(* else *)
  r = rkm/MeV$km;
  enu = e$to$the$nu[tov, r];
  elambda = e$to$the$lambda[tov, r];
  pr = 0;
  edens = 0;
  elambda^3 * enu * r^(-2) * (edens + pr)
 ]
];

W$of$r[tov_?is$TOV, r_?NumericQ]:=Module[{enu, elambda, pr, edens},
 If[r <= radius[tov],
  enu = e$to$the$nu[tov, r];
  elambda = e$to$the$lambda[tov, r];
  pr = p$of$r[tov, r];
  edens = e$of$r[tov, r];   
 ,(* else *)
  enu = e$to$the$nu[tov, r];
  elambda = e$to$the$lambda[tov, r];
  pr = 0;
  edens = 0;
 ];
 enu * elambda^3 * r^(-2) * (edens + pr)
];

W$of$r$times$rMeV3[tov_?is$TOV, r_?NumericQ]:=Module[{enu, elambda, pr, edens},
 If[r <= radius[tov],
  enu = e$to$the$nu[tov, r];
  elambda = e$to$the$lambda[tov, r];
  pr = p$of$r[tov, r];
  edens = e$of$r[tov, r];   
 ,(* else *)
  enu = e$to$the$nu[tov, r];
  elambda = e$to$the$lambda[tov, r];
  pr = 0;
  edens = 0;
 ];
  enu * elambda^3 * r * (edens + pr)
];

Q$of$r[tov_?is$TOV, r_?NumericQ] := Module[{elambda, enu, dp$dr, edens, pr},
 If[r <= radius[tov],
  elambda = e$to$the$lambda[tov, r];
  enu = e$to$the$nu[tov, r];
  dp$dr = dpdr[tov, r]; 
  pr = p$of$r[tov, r]; 
  edens = e$of$r[tov, r];
,(* else *)
  elambda = lambda$of$rkm[tov, r];
  enu = nu$of$rkm[tov, r]; 
  dp$dr = 0;
  pr = 0;
  edens = 0;
];
 -4 * elambda * enu^3 * r^(-3) * dp$dr 
  - 8 * Pi * NewtonG * elambda^3 * enu^3 * r^(-2) * pr * (edens + pr)
  + elambda * enu^3 * r^(-2) * (edens + pr)^(-1) * dp$dr^2
];

Q$of$r$times$rMeV3[tov_?is$TOV, r_?NumericQ] := Module[{elambda, enu, dp$dr, edens, pr},
 If[r <= radius[tov],
  elambda = e$to$the$lambda[tov, r];
  enu = e$to$the$nu[tov, r];
  dp$dr = dpdr[tov, r]; 
  pr = p$of$r[tov, r]; 
  edens = e$of$r[tov, r];
,(* else *)
  elambda = lambda$of$rkm[tov, r];
  enu = nu$of$rkm[tov, r]; 
  dp$dr = 0;
  pr = 0;
  edens = 0;
 ];
 -4 * elambda * enu^3 * dp$dr 
  - 8 * Pi * NewtonG * elambda^3 * enu^3 * r * pr * (edens + pr)
  + elambda * enu^3 * r * (edens + pr)^(-1) * dp$dr^2
];

Q$of$rkm[tov_?is$TOV,rkm_?NumericQ]:=Module[{elambda, enu, r, dp$dr, edens, pr, pr$km, edens$km},
 If[rkm <= radius$km[tov],
  r = rkm/MeV$km;
  elambda = e$to$the$lambda[tov, r];
  enu = e$to$the$nu[tov, r];
  dp$dr = dpdr[tov, r]; 
  pr = p$of$r[tov, r]; 
  edens = e$of$r[tov, r];
,(* else *)
  r = rkm/MeV$km;
  elambda = e$to$the$lambda[tov, r];
  enu = e$to$the$nu[tov, r];
  dp$dr = 0; 
  pr = 0; 
  edens = 0;
];
 -4 * elambda * enu^3 * r^(-3) * dp$dr 
  - 8 * Pi * NewtonG * elambda^3 * enu^3 * r^(-2) * pr * (edens + pr)
  + elambda * enu^3 * r^(-2) * (edens + pr)^(-1) * dp$dr^2
];


Options[solve$TOV]={Pfrac->0.0001,Psurface->0};
solve$TOV[tov_?is$TOV,OptionsPattern[]]:=Module[
 {r,p,pr,Mr,M,rules,Npts,i,messages,r1,pr1,Mr1,
 p$init,M$init,r$init,p$final,drdp},
 (* TOV equation: *)
 drdp[r1_?NumericQ,pr1_?NumericQ,Mr1_?NumericQ]:=Module[{},
  (*Print[CForm[r1],"  ",CForm[pr1],"  ",CForm[Mr1],
   "  ",CForm[Edensity[tov][pr1]]];*)
  -r1*(r1-2*NewtonG*Mr1)/( 
    NewtonG*(Edensity[tov][pr1] + pr1)*(Mr1 + 4*Pi*r1^3*pr1) 
  )
 ];
 messages=False;  
 (* messages=True;  uncomment this for message printing *)
 (* 
 If[messages, Print["solve$TOV: Checking causality."]];
  check$causal$pvals[tov];
 *)
 (* We first evolve using the approximate TOV eqn from
    p$central to (1-pfrac1)*p$central
 *)
 If[messages, Print["solve$TOV: Initial evolution."]];
 p$init=(1-OptionValue[Pfrac])*p$central[tov];
 r$init=r$approx[tov,p$init];
 M$init=4/3*Pi*Edensity[tov][p$central[tov]]*r$init^3;
 set$p$int[tov,p$init];
 set$r$int[tov,r$init];
 set$M$int[tov,M$init];
 If[OptionValue[Psurface]<=0,
  p$final=p$surface[tov]
 ,(*else*)
  p$final=OptionValue[Psurface]
 ];
 If[messages, Print["solve$TOV: Numerical Solution"]];
 Off[NDSolve::precw];  (* Turn off stupid warning *)
 rules = NDSolve[
 { r'[p] == drdp[r[p],p,M[p]],
   M'[p] == 4*Pi*r[p]^2*Edensity[tov][p]*drdp[r[p],p,M[p]],
   M[p$init]==M$init, r[p$init]==r$init
 },
   {r,M}, {p,p$init,p$final}, WorkingPrecision->20
 ];
 On[NDSolve::precw]; 
 (* really, rather than WorkingPrecision->20, we should do an analytic
    solution for the lowest pressure region as well, using an assumed
    power law for for the Edensity function. *)
 If[messages, Print["solve$TOV: incorporating results"]];
 set$p$surface[tov,p$final];
 set$r$of$p$IF[tov,r /. rules[[1]]];
 set$p$of$r$IF[tov, InverseInterpolatingFunction[r$of$p$IF[tov]] ];
 set$M$of$p$IF[tov,M /. rules[[1]]];
 If[ ! in$domain[r$of$p$IF[tov],p$final], Abort[] ]; 
 set$radius[tov,r$of$p$IF[tov][p$final]];
 If[ ! in$domain[M$of$p$IF[tov],p$final], Abort[] ];
 set$mass[tov,M$of$p$IF[tov][p$final]];
 set$radius$km[tov, radius[tov]*MeV$km];
 set$mass$solar[tov, mass[tov]/MSolar$MeV];
];


RF[x_,n_?IntegerQ]:=CForm[SetPrecision[x,n]]; (* just a local abbreviation *)

PrintProperties[tov_?is$TOV]:=Module[{},
 Print["P$central= ",RF[p$central[tov],6]," MeV^4"];
 Print["  M= ",RF[mass$solar[tov],4]," Msolar"];
 Print["  R= ",RF[radius$km[tov],4]," km"];
];

p$approx[tov_?is$TOV,r_]:=Module[{kappa},
 (* approx soln for p[r], valid at small r *)
 kappa=4*Pi*NewtonG*(p$central[tov] + Edensity[tov][p$central[tov]])*(p$central[tov] + Edensity[tov][p$central[tov]]/3);
 p$central[tov]-kappa*r^2/2
];

M$approx[tov_?is$TOV,r_]:=Module[{kappa},
 (* approx soln for M[r], valid at small r *)
 4/3*Pi*r^3*Edensity[tov][p$central[tov]]
];

r$approx[tov_?is$TOV,p_]:=Module[{kappa},
 (* approx soln for r[p], valid at small r *)
 kappa=4*Pi*NewtonG*(p$central[tov] + Edensity[tov][p$central[tov]])*(p$central[tov] + Edensity[tov][p$central[tov]]/3);
 Sqrt[2/kappa*(p$central[tov]-p)]
];

plot$TOV[tov_?is$TOV,range$km_:{Null,Null}]:=Module[
 {rkm,r$km,pfn,Mfn,rmin$km,rmax$km},
 pfn[rkm_]:= p$of$r[tov,rkm/MeV$km];
 Mfn[rkm_]:= M$of$r[tov,rkm/MeV$km];
 If[IntegerQ[range$km[[1]]],
  rmin$km=range$km[[1]]
 ,(*else*)
  rmin$km=0
 ];
 If[IntegerQ[range$km[[2]]],
  rmax$km=range$km[[2]]
 ,(*else*)
  rmax$km=radius$km[tov]
 ];
 p$plot=LogPlot[
  pfn[r$km],{r$km,rmin$km,rmax$km},AxesOrigin->{0,0},PlotRange->All]; 
 M$plot=Plot[
  Mfn[r$km],{r$km,rmin$km,rmax$km},AxesOrigin->{0,0},PlotRange->All]; 
];

Options[probe$TOV]={dr->0.01/MeV$km};
probe$TOV[tov_?is$TOV,rkm_,OptionsPattern[]]:=Module[
 {r1,p1,m1,pr,r,dpdr$soln,dpdr$TOV,d$r},
 r1=rkm/MeV$km;
 p1=p$of$r[tov,r1];
 m1=M$of$r[tov,r1];
 Print["r= ", RF[r1,6], " MeV^-1"];
 Print["p= ", RF[p1,6], " MeV^4"];
 Print["m= ", RF[m1,6], " MeV"];
 Print["e= ", RF[Edensity[tov][p1],6], " MeV^4"];
 (* Print["r^2*dp/dr= ", RF[r1^2*dpdr[tov,r1],6], " MeV^3"]; *)
 dpdr$soln=ND[p$of$r[tov,r],r,r1];
 d$r=OptionValue[dr];
 dpdr$soln= (p$of$r[tov,r1+d$r]-p$of$r[tov,r1-d$r])/(2*d$r);
 dpdr$TOV=-NewtonG*(Edensity[tov][p1] + p1)*(m1 + 4*Pi*r1^3*p1) / (
  r1*(r1-2*NewtonG*m1) );
 Print["dpdr(soln)= ",RF[dpdr$soln,8], " MeV^5"]
 Print[" dpdr(TOV)= ",RF[dpdr$TOV,8], " MeV^5"]
 Print[" dp/dr(fn)= ", RF[dpdr[tov,r1],8], " MeV^5"];
];


check$causal[tov_?is$TOV,pr_?NumericQ]:=Module[{p0,dp,vsq,de},
 ep$fn=Edensity[tov];
 dp=1*^-6*pr;
 de=ep$fn[pr+dp]-ep$fn[pr];
 If[de==0,
  Print["TOV`check$causal: energy density is indp of pressure,"];
  Print["  so vsq=Infinity at p= ",pr];
  Return[False];
 ,(*else*)
  vsq= dp/de
 ];
 If[vsq>1,
  Print["TOV`check$causal: v^2= ",vsq," at p= ",pr]
 ];
 Return[vsq<=1]
];

check$causal$pvals[tov_?is$TOV]:=Module[{p$max,p$min,n$check,i},
 p$max=p$central[tov];
 p$min=Min[p$central[tov]/100.0, 1.0]; 
   (* don't want p$min==p$max *)
 n$check=20;
 For[i=n$check, i>=1, i--,
  p=(p$max/p$min)^(i/n$check) * p$min; 
  If[ ! check$causal[tov,p], Break[] ];
 ];
];

ClassDef`EndClass[];  (* Note you need the explicit ClassDef` context *)


(*
Usage example: 

Needs["TOV`"];

"define Equation of State:";
bag$const=145.0^4;
edens[pr_?NumericQ] :=  3*pr + 4* bag$const;

"create TOV object:";
tov1 = create$TOV[ 1.0*^10, edens]; solve$TOV[tov1];

"check its mass and radius:";
PrintProperties[tov1];
{radius$km[tov1],mass$solar[tov1]}

"check some other properties:";
dpdr[tov1, 4.0/MeV$km]        -2.96875*^-7
e$of$r[tov1, 4.0/MeV$km]     1.03479*^10

Plot[ p$of$r[tov1,r/MeV$km], {r,0,radius$km[tov1]} ]

"survey a family of stars with various p_central:";
MR$list={};
p=1*^10;
For[n=1, n<=10, n++,
 p=p/2;
 tov=create$TOV[ p, edens]; solve$TOV[tov];
 MR$list=Append[MR$list,{radius$km[tov],mass$solar[tov]}];
];

*)


(*
"test check$causal:";
edens[pr_?NumericQ] :=  If[pr<1*^6, 0, 0.9*(pr-1*^6)];
tov2 = create$TOV[ 2.0*^10, edens]; 
solve$TOV[tov2];
*)

