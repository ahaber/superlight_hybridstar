(* ::Package:: *)

(*Package for creating the constant speed of sound EOS from Alford, Han https://arxiv.org/pdf/1302.4732.pdf , modified by AHaber 6th Jan 2021*)
Needs["ClassDef`"];

ClassDef`BeginClass["CSS",
 (* Needed packages: *)
 {},

 (* data members: *)
 {"e0",
  "n0",
  "mu0",
  "csq",
  "A", "B", "beta"
 },

 (* member functions (other than create$TOV and is$TOV): *)
 {"PrintProperties", 
 "p$of$mu$CSS",
 "e$of$mu$CSS",
 "n$of$mu$CSS",
 "mu$of$p$CSS",
 "e$of$p$CSS",
 "n$of$p$CSS",
 "p$of$nb$CSS",
 "e$of$nb$CSS",
 "mu$of$nB$CSS"
 }
];

create$CSS[e0$val_,n0$val_,csq$val_]:=Module[
 {self,beta$val,mu0$val},
 self=create$CSS[]; (* call default constructor, defined in ClassDef *)
 mu0$val=e0$val/n0$val;
 beta$val=1/csq$val;   (* Alford&Han, Appendix A *)
 set$e0[self,e0$val];  (* energy density at P=0 *)
 set$n0[self,n0$val];
 set$mu0[self,mu0$val]; 
 set$csq[self,csq$val];
 set$beta[self,beta$val];
 set$B[self,e0$val/(1+beta$val)];
 set$A[self,n0$val/(1+beta$val)/(mu0$val^beta$val)];
 self
];

p$of$mu$CSS[css_?is$CSS,mu_?NumericQ]:=A[css]*mu^(1+beta[css]) - B[css];

e$of$mu$CSS[css_?is$CSS,mu_?NumericQ]:=B[css] + beta[css]*A[css]*mu^(1+beta[css]);

n$of$mu$CSS[css_?is$CSS,mu_?NumericQ]:=(1 + beta[css])*A[css]*mu^beta[css];

mu$of$p$CSS[css_?is$CSS,p_?NumericQ]:= ((p+B[css])/A[css])^(csq[css]/(1+csq[css]));

mu$of$nB$CSS[css_?is$CSS,nb_?NumericQ]:= (nb/(A[css]*(1+beta[css])))^(1/beta[css]);

e$of$p$CSS[css_?is$CSS,p_?NumericQ]:= e0[css] + p/csq[css];

e$of$nb$CSS[css_?is$CSS,nb_?NumericQ]:= e$of$mu$CSS[css,mu$of$nB$CSS[css,nb]];

n$of$p$CSS[css_?is$CSS,p_?NumericQ]:= (1+beta[css])*A[css]*(((p+B[css])/A[css])^(1/(1+csq[css])));
p$of$nb$CSS[css_?is$CSS,nb_?NumericQ]:=p$of$mu$CSS[css,mu$of$nB$CSS[css,nb]];

ClassDef`EndClass[];  (* Note you need the explicit ClassDef` context *)


(*
Usage example: 

Needs["CSS`"];
Needs["Constants`"];

css1 = create$CSS[NuclearEnergyDensity$MeV4, NuclearDensity$nucleons$MeV3,0.5];

Plot[ e$of$p$CSS[css1,p], {p,0,100^4} ];

Plot[ e$of$p$CSS[css1,p] - Mneutron$MeV*n$of$p$CSS[css1,p], {p,0,100^4} ]

Plot[ mu0[css1]*n$of$mu$CSS[css1,mu] - e$of$mu$CSS[css1,mu], {mu,mu0[css1],2*mu0[css1]} ]
OK, this is mostly negative, indicating that the matter is ... bound?

Plot[ { mu*n$of$mu$CSS[css1,mu] - e$of$mu$CSS[css1,mu],
       p$of$mu$CSS[css1,mu]}, {mu,mu0[css1],2*mu0[css1]} ]

These agree (good) and mostly positive, indicating that the pressure is going up.

"comment";

*)

