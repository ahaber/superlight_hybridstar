(* ::Package:: *)

(* ::Title:: *)
(*Tools to Test Points/Curves in/trough Polygons*)


(*written by Alexander Haber Dec. 2022, ahaber@physics.wustl.edu*) (*this code might fail for points ON a polygon POINT*)


BeginPackage["in$polygon$check`"];
(*public functions*)
{points$in$polygon,interpolation$intersect,box$test$interpolation}
Begin["`Private`"]


(* ::Chapter:: *)
(*Test Which Points are in a Polygon*)


(*this code uses ray tracing in 2D to test whether a given set of points is inside a polygon. On the line counts as in. See https://en.wikipedia.org/wiki/Point_in_polygon*)


(*This undocumented code works as well: inPolyQ[poly_,pt_]:=Graphics`PolygonUtils`PointWindingNumber[poly,pt]=!=0 *)


(* ::Input::Initialization:: *)
(*this function is a pretest: it creates a rectangle that completely encompasses the polygon. If a point is not in this box it can NOT be in the polygon, so we are drastically reduce the amounts of points we need to check*)
box$test[polygon_,points_]:=Module[{points$in,point,x$extrema,y$extrema,points$out,i},
x$extrema=MinMax[polygon[[All,1]]];(*search for extrema of polygon in order to create box*)
y$extrema=MinMax[polygon[[All,2]]];
points$in={};
points$out={};
Do[( (*scan trough points*)
point=points[[i]];If[point[[1]]<=x$extrema[[2]]&&point[[1]]>=x$extrema[[1]]&&point[[2]]<=y$extrema[[2]]&&point[[2]]>=y$extrema[[1]],points$in=AppendTo[points$in,{point}],points$out=AppendTo[points$out,{point}]];
),{i,1,Length[points]}];
Return[{points$in,points$out}](*returns points in and out of the BOX*)
]



(* ::Input::Initialization:: *)
(*this code uses ray tracing to determine whether a point is inside (odd number of intersections with polygon) or outside*)
points$in$polygon[polygon_,allpoints_]:=Module[{ray,polyray,x1,x2,xsol,ysol,i,sol,point$in$tab,point$out$tab,points,polyxmin,ray$norm,poly$norm,rayvec,polyvec,boxres,polymin,polymax,intersections},
(*reduce number of points with box check*)
boxres=box$test[polygon,allpoints]; 
points=Flatten[boxres[[1]],1];
(*create straight lines for polygon side and ray of point*)
ray[x_,i_]:=points[[i]]+x*({-1,0}); (*cast ray to the left*)
polyvec[j_]:=(polygon[[j+1]]-polygon[[j]])/Norm[(polygon[[j+1]]-polygon[[j]])];
polyray[x_,j_]:=polygon[[j]]+x*polyvec[j];
(*loop trough points and polygon sides*)
point$in$tab={};
point$out$tab={};
Do[( 
intersections=0;
Do[( 
If[Abs[polyvec[j]]!={1,0}(*check if colinear*),
(*compute intersection*)
sol=Solve[ray[x1,i]==polyray[x2,j],{x1,x2}];
xsol=polyray[(x2/.sol)[[1]],j][[1]];
ysol=polyray[(x2/.sol)[[1]],j][[2]];
(*compute endpoints of polygon line*)
polymin=Min[polygon[[j,2]],polygon[[j+1,2]]];
polymax=Max[polygon[[j,2]],polygon[[j+1,2]]];
(*check whether intersection is inside boundaries of polygon line*)
If[xsol<=points[[i,1]]&&ysol>=polymin&&ysol<=polymax,intersections=intersections+1]];
),{j,1,Length[polygon]-1}];
(*if number intersections = Odd -> point is in, otherwise->out*)
If[OddQ[intersections],point$in$tab=AppendTo[point$in$tab,{points[[i]]}],point$out$tab=AppendTo[point$out$tab,{points[[i]]}]]
),{i,1,Length[points]}
];
Return[{point$in$tab,Join[point$out$tab,boxres[[2]]]}](*return list of points inside and outside*)
];




(* ::Input:: *)
(**)


(* ::Chapter:: *)
(*Test if Interpolated Curve runs through Polygon*)


(*In case the points are so sparse that no point is inside the polygon although the line runs trough it we test whether a linear interpolation intersects with the polygon*)


(* ::Input::Initialization:: *)
(*reduces the polygon to a box so we can run a quicker test with just the 4 sides*)
box$test$interpolation[polygon_,curve_]:=Module[{point,x$extrema,y$extrema,x$extrema$ray,y$extrema$ray,i,curve$ray,intersections=0,intersects=False,curve$vec,s,t,(*sol,x1,x2,xsol,ysol,*)boxpoints,boxvec},
(*compute box*)
x$extrema=MinMax[polygon[[All,1]]];
y$extrema=MinMax[polygon[[All,2]]];
boxpoints={{x$extrema[[1]],y$extrema[[1]]},{x$extrema[[1]],y$extrema[[2]]},{x$extrema[[2]],y$extrema[[2]]},{x$extrema[[2]],y$extrema[[1]]}};
boxpoints=AppendTo[boxpoints,boxpoints[[1]]];
(*create vectors/lines of box and rays*)
boxvec[j_]:=(boxpoints[[j+1]]-boxpoints[[j]])/Norm[boxpoints[[j+1]]-boxpoints[[j]]];
curve$vec[i_]:=(curve[[i+1]]-curve[[i]])/Norm[(curve[[i+1]]-curve[[i]])];
curve$ray[s_,i_]:=curve[[i]]+s*curve$vec[i];
(*loop trough 4 sides and all linear lines that connect two neighbouring points*)
Do[
Do[( 
x$extrema$ray=MinMax[{curve[[i,1]],curve[[i+1,1]]}];
y$extrema$ray=MinMax[{curve[[i,2]],curve[[i+1,2]]}];
If[Abs[curve$vec[i]]!=Abs[boxvec[j]](*check if collinear*),
(*compute intersection*)
sol=Solve[curve$ray[x1,i]==boxpoints[[j]]+x2*boxvec[j],{x1,x2}];
xsol=curve$ray[(x1/.sol)[[1]],i][[1]];
ysol=curve$ray[(x1/.sol)[[1]],i][[2]];
(*check if intersection is within polygon side*)
If[xsol>=x$extrema[[1]]&&xsol<=x$extrema[[2]]&&ysol>=y$extrema[[1]]&&ysol<=y$extrema[[2]]&&xsol>=x$extrema$ray[[1]]&&xsol<=x$extrema$ray[[2]]&&ysol>=y$extrema$ray[[1]]&&ysol<=y$extrema$ray[[2]],intersections=intersections+1]
]
),{i,1,Length[curve]-1}];,{j,1,4}];(*if intersects at all we call it True*)
If[intersections>0,intersects=True,intersects=False];
Return[{intersects,intersections}]
]


(* ::Input:: *)
(**)


(* ::Input::Initialization:: *)
(*same module as above but now we run trough all polygon sides*)
interpolation$intersect[polygon_,curve_]:=Module[{point,x$extrema,y$extrema,i,curve$ray,intersections=0,intersects=False,curve$vec,s,t,sol,x1,x2,xsol,ysol,boxtrue,polyvec,x$extrema$ray,y$extrema$ray},
(*reduce parameter space by running box test*)
(*boxtrue=box$test$interpolation[polygon,curve][[1]];*)
boxtrue=True;
If[boxtrue,
polyvec[j_]:=(polygon[[j+1]]-polygon[[j]])/Norm[polygon[[j+1]]-polygon[[j]]];
curve$vec[i_]:=(curve[[i+1]]-curve[[i]])/Norm[(curve[[i+1]]-curve[[i]])];
curve$ray[s_,i_]:=curve[[i]]+s*curve$vec[i];
Do[
x$extrema=MinMax[{polygon[[j,1]],polygon[[j+1,1]]}];
y$extrema=MinMax[{polygon[[j,2]],polygon[[j+1,2]]}];
Do[( 
x$extrema$ray=MinMax[{curve[[i,1]],curve[[i+1,1]]}];
y$extrema$ray=MinMax[{curve[[i,2]],curve[[i+1,2]]}];
If[Abs[curve$vec[i]]!=Abs[polyvec[j]],
sol=Solve[curve$ray[x1,i]==polygon[[j]]+x2*polyvec[j],{x1,x2}];
xsol=curve$ray[(x1/.sol)[[1]],i][[1]];
ysol=curve$ray[(x1/.sol)[[1]],i][[2]];
If[xsol>=x$extrema[[1]]&&xsol<=x$extrema[[2]]&&ysol>=y$extrema[[1]]&&ysol<=y$extrema[[2]]&&xsol>=x$extrema$ray[[1]]&&xsol<=x$extrema$ray[[2]]&&ysol>=y$extrema$ray[[1]]&&ysol<=y$extrema$ray[[2]],intersections=intersections+1]
]
),{i,1,Length[curve]-1}];,{j,1,Length[polygon]-1}];
If[intersections>0,intersects=True,intersects=False];,intersects=False];
Return[{intersects,intersections}]
]


End[];
EndPackage[];


(* sample code*)(*
testpolygon={{0,-0},{-1,0},{0,1},{1,0.7}};
testpolygon=AppendTo[testpolygon,testpolygon[[1]]]
testpoints=Table[{RandomReal[{-2,2}],RandomReal[{-2,2}]},{i,1,500}];
Show[ListPlot[testpoints],ListLinePlot[testpolygon,PlotStyle\[Rule]Red]]
test1=box$test[testpolygon,testpoints];
Show[ListPlot[Flatten[test1[[2]],1],PlotStyle\[Rule]Black],ListPlot[Flatten[test1[[1]],1],PlotStyle\[Rule]Green],ListLinePlot[testpolygon,PlotStyle\[Rule]Red]]

test=points$in$polygon[testpolygon,testpoints];
Show[{ListPlot[Flatten[test[[2]],1],PlotStyle\[Rule]Black,PlotRange\[Rule]All],ListLinePlot[testpolygon,PlotStyle\[Rule]Red],ListPlot[Flatten[test[[1]],1],PlotStyle\[Rule]Green]},PlotRange->All]

testcurve={{0.5,0},{-0.5,1.5}};
testcurveout={{1.5,0},{1.5,1.5}};

interpolation$intersect[testpolygon,testcurve]
Show[{ListLinePlot[testcurve,PlotMarkers\[Rule]Automatic],ListLinePlot[{testpolygon},PlotStyle\[Rule]Red]},PlotRange->All]
*)
